
import re
from webbrowser import get 
import ipaddress
route_info = {}
mask_info = []

#print the output
output = open("destination_output.txt", mode = "w")

def print_destination (destination, nexthop):
	if nexthop[0] == '-': 
		output.write("Packet with destination address " + str(destination) + " is connected directly and out on interface " + str(nexthop[1]))
	else:
		output.write("Packet with destination address " + str(destination) + " will be forwarded to "
           + str(nexthop[0]) + " out on interface " + str(nexthop[1]))	
	output.write("\n")

def findtheroute():
	for row in data:
		rows = row.split(".")
		#Longest mask
		if mask_info.__contains__("255.255.255.255"):		
			destination = route_info.get(row)
			if destination != None:
				print_destination(row, destination.split(','))
				continue
		if mask_info.__contains__("255.255.255.0"):
			destination = route_info.get(rows[0] + "." + rows[1] + "." + rows[2] + ".0")
			if destination != None:
				print_destination(row, destination.split(','))		
				continue
		if mask_info.__contains__("255.255.0.0"):
			destination = route_info.get(rows[0] + "." + rows[1] + ".0.0")
			if destination != None:
				print_destination(row, destination.split(','))
				continue
		if mask_info.__contains__("255.0.0.0"):
			destination = route_info.get(rows[0] + ".0.0.0")
			if destination != None:
				print_destination(row, destination.split(','))
				continue
		if mask_info.__contains__("0.0.0.0"):
			destination = route_info.get("0.0.0.0")
			print_destination(row, destination.split(','))
			continue

#read the file content
with open("routing_table_input.txt", mode="r") as file:
    data = file.read()
    data = re.sub('[^a-zA-Z0-9\\n,-.]', ' ', data).split('\n')
#populate the route dictionary with next hop information for destination
for row in data:
    columns = row.split(',')
    route_info[columns[1].strip()] =  columns[2].strip() + "," + columns[4].strip()
    mask_info.append(columns[0].strip())

with open("datagram_packerts_input.txt", mode="r") as file:
     data = file.read()
     data = re.sub('[^a-zA-Z0-9\\n.]', ' ', data).split('\n')

findtheroute()

